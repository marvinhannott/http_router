## 1.0.0-beta

- Initial beta version

## 2.0.0-beta

- completely switched the implementation to a more elegant design

## 2.0.1-beta

- fix! When no suitable Handler for a `HttpRequest`'s method was found, code 400 (Bad Request) was send to the client, which is inappropriate; 
instead code 405 (Method Not Allowed) will be send now. 