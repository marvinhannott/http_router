List<String> filterTrailingSlash(List<String> segments) {
  if (segments.isEmpty || segments.last.isNotEmpty) {
    return segments;
  }
  return segments.sublist(0, segments.length - 1);
}
