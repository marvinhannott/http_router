import 'dart:collection';

import 'package:path/path.dart' as pathlib;

class Node<T> {
  final String segment;
  T value;
  final nextSegments = HashMap<String, Node<T>>();
  Node<T> wildcard;
  T catchAll;
  Node(this.segment);

  @override
  String toString() {
    return 'Node($segment, $value, $nextSegments)';
  }
}

class PathTree<T> {
  Node<T> head = Node('');

  void insert(List<String> segments, T value) {
    if (segments.isEmpty) {
      head.value = value;
      return;
    }

    var currentNode = head;
    for (final segment in segments) {
      if (segment == '*') {
        currentNode.wildcard ??= Node('*');
        currentNode = currentNode.wildcard;
      } else if (segment == '**') {
        currentNode.catchAll ??= value;
      } else {
        var nextNode = currentNode.nextSegments[segment];
        if (nextNode == null) {
          nextNode = Node<T>(segment);
          currentNode.nextSegments[segment] = nextNode;
        }
        currentNode = nextNode;
      }
    }
    currentNode.value = value;
  }

  T get(List<String> segments) {
    if (segments.isEmpty) {
      return head.value;
    }
    var currentNode = head;
    var catchAll = head.catchAll;
    for (final segment in segments) {
      final nextNode =
          currentNode.nextSegments[segment] ?? currentNode.wildcard;
      if (nextNode == null) {
        return catchAll;
      }
      catchAll = nextNode.catchAll ?? catchAll;
      currentNode = nextNode;
    }
    return currentNode.value;
  }

  @override
  String toString() {
    return head.toString();
  }
}
