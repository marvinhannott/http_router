import 'dart:io';
import 'package:simple_http_router/simple_http_router.dart';

const port = 3000;
const address = 'localhost';

void indexHandler(HttpRequest req, [Context ctx]) {
  req.response.writeln('Hello World');
  req.response.close();
}

void setCookieMiddleware(HttpRequest req, [Context con]) {
  req.response.cookies.add(Cookie('session', 'visited'));
}

void notFoundHandler(HttpRequest req, Context con) {
  req.response.writeln('waefjioawefwef');
  req.response.close();
}

void main(List<String> args) async {
  final server = await HttpServer.bind(address, port);
  final router = Router();

  router.register('/index', HttpMethodHandler(get: indexHandler));

  router.register(
      '/cookie/', MiddlewareHandler(const [setCookieMiddleware, indexHandler]));

  router.registerNotFound('/index', notFoundHandler);

  print('listen on http://$address:$port');
  await for (final request in server) {
    router.handle(request);
  }
}
