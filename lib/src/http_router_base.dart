import 'dart:async';
import 'dart:io';
import 'pathtree.dart';
import 'path.dart';

/// A [Handler] can handle [HttpRequest] specific operations
/// or act as middleware.
typedef Handler = FutureOr<void> Function(HttpRequest, Context);
typedef MethodNotAllowedHandler = FutureOr<void> Function(
    HttpRequest, List<String> allowedMethods, Context);

/// Context for the current route pipeline.
/// The [Context] gets passed from middleware to middleware.
/// Anything that gets passed to [values] will be visibel to the other middleware.
/// Calling [cancel] prevents any further middlewere from executing.
/// See [MiddlewareHandler].
class Context {
  var _canceled = false;
  bool get canceled => _canceled;
  final values = <String, dynamic>{};
  Context();
  void cancel() {
    _canceled = true;
  }
}

/// Executes [Handler]s as a sequetial route pipeline.
class MiddlewareHandler {
  final List<Handler> _handlers;

  MiddlewareHandler(this._handlers)
      : assert(_handlers.isNotEmpty,
            '_handlers should contain at least one Handler');

  FutureOr<void> call(HttpRequest request, Context context) async {
    for (final handler in _handlers) {
      if (context.canceled) return;
      await handler(request, context);
    }
  }
}

/// [MiddlewareBuilder] makes it less tedious to add the same middleware to multiple [Handler]s.
/// See [MiddlewareHandler].
class MiddlewareBuilder {
  final List<Handler> _before, _after;

  /// [before] and [after] are the middleware that gets executed before and after the route specific [Handler].
  const MiddlewareBuilder(
      {List<Handler> before = const [], List<Handler> after = const []})
      : _before = before,
        _after = after;

  /// [handler] is the route specific [Handler].
  /// If [handler] is a [MiddlewareHandler], it gets copied.
  MiddlewareHandler build(Handler handler) {
    List<Handler> middlewareHandlers;
    if (handler is MiddlewareHandler) {
      middlewareHandlers = [...(handler as MiddlewareHandler)._handlers];
    } else {
      middlewareHandlers = [handler];
    }
    return MiddlewareHandler([..._before, ...middlewareHandlers, ..._after]);
  }

  /// [handlers] is middleware plus a route specific [Handler].
  MiddlewareHandler buildAll(List<Handler> handlers) {
    return MiddlewareHandler([..._before, ...handlers, ..._after]);
  }
}

/// Handles the HTTP Method specific [Handler]s.
class HttpMethodHandler {
  List<String> _allowedMethods;

  final Handler _get,
      _post,
      _delete,
      _update,
      _push,
      _connect,
      _options,
      _trace,
      _patch;
  final MethodNotAllowedHandler _methodNotAllowed;

  /// [methodNotAllowed] gets executed when no [Handler] for that method has been registered.
  HttpMethodHandler(
      {Handler get,
      Handler delete,
      Handler update,
      Handler post,
      Handler push,
      Handler connect,
      Handler options,
      Handler trace,
      Handler patch,
      MethodNotAllowedHandler methodNotAllowed = defaultMethodNotAllowed})
      : _get = get,
        _post = post,
        _delete = delete,
        _update = update,
        _push = push,
        _connect = connect,
        _options = options,
        _trace = trace,
        _patch = patch,
        _methodNotAllowed = methodNotAllowed {
    _allowedMethods = [
      if (get != null) 'GET',
      if (delete != null) 'DELETE',
      if (update != null) 'UPDATE',
      if (post != null) 'POST',
      if (push != null) 'PUSH',
      if (connect != null) 'CONNECT',
      if (options != null) 'OPTIONS',
      if (trace != null) 'TRACE',
      if (patch != null) 'PATCH',
    ];
  }

  FutureOr<void> call(HttpRequest request, Context context) async {
    Handler handler;
    switch (request.method) {
      case 'GET':
        handler = _get;
        break;
      case 'POST':
        handler = _post;
        break;
      case 'UPDATE':
        handler = _update;
        break;
      case 'DELETE':
        handler = _delete;
        break;
      case 'PUSH':
        handler = _push;
        break;
      case 'CONNECT':
        handler = _connect;
        break;
      case 'OPTIONS':
        handler = _options;
        break;
      case 'TRACE':
        handler = _trace;
        break;
      case 'PATCH':
        handler = _patch;
        break;
      default:
        throw HttpException(
            'Http Method \'${request.method}\' is not supported');
        return;
    }
    if (handler == null) {
      _methodNotAllowed(request, _allowedMethods, context);
      return;
    }
    await handler(request, context);
  }
}

FutureOr<void> defaultNotFound(HttpRequest req, Context context) {
  req.response.statusCode = HttpStatus.notFound;
  req.response.writeln(req.response.reasonPhrase);
  req.response.close();
}

FutureOr<void> defaultMethodNotAllowed(
    HttpRequest request, List<String> allowedMethods, Context context) {
  request.response.statusCode = HttpStatus.methodNotAllowed;
  request.response.headers.add(HttpHeaders.allowHeader, allowedMethods);
  //ignore:unawaited_futures
  request.response.close();
  context.cancel();
}

/// Matches incoming [HttpRequests] against registered [Routes].
///
/// NOTE: [Routes] get matched in the order they are registered.
/// Overshadowing with wildcards or reregistering a route causes a [StateError]
class Router {
  final PathTree<Handler> _routes = PathTree();
  final Handler _notFound;

  /// [notFound] gets executed when no route matches the [HttpRequest]
  Router({Handler notFound = defaultNotFound}) : _notFound = notFound;

  /// Checks if [route] would be overshadowed by an already registered [Route]
  void _routeOvershadowed(Uri uri) {
    final overshadowingRoute = _routes.get(uri.pathSegments);
    if (overshadowingRoute != null) {
      throw StateError('Route ${uri} has already been registered.');
    }
  }

  static bool _checkWildcards(Uri uri) {
    for (final seg in uri.pathSegments) {
      if (seg.length > 1 && seg.contains('*')) {
        throw FormatException(
            'Wildcard must be a single \'*\' per path segment', uri.path);
      }
    }
    return true;
  }

  void _register(Uri path, Handler methodHandler) {
    _routeOvershadowed(path);
    final filteredSegments = filterTrailingSlash(path.pathSegments);
    _routes.insert(filteredSegments, methodHandler);
  }

  /// [path] must be parsable by [Uri.parse]. It can contain a single star `*` as wildcard
  /// for path segments like `/user/*/edit`, where the `/*/` segment could represent a specific user id.
  /// Invalid use of wildcards results in a [FormatException].
  void register(String path, Handler methodHandler) {
    final uri = Uri.parse(path);
    _checkWildcards(uri);
    _register(uri, methodHandler);
  }

  void registerAll(List<String> paths, Handler methodHandler) {
    for (final path in paths) {
      register(path, methodHandler);
    }
  }

  /// [handler] gets executed when a subpath of [path] doesn't exist.
  void registerNotFound(String path, Handler handler) {
    final uri = Uri.parse(path);
    _checkWildcards(uri);
    final matchAnyUri = uri.resolve('**');
    _register(matchAnyUri, handler);
  }

  void registerAllNotFound(List<String> paths, HttpMethodHandler handler) {
    for (final path in paths) {
      registerNotFound(path, handler);
    }
  }

  /// Handle incoming [HttpRequest]
  FutureOr<void> handle(HttpRequest request) async {
    final context = Context();
    final filteredSegments = filterTrailingSlash(request.uri.pathSegments);
    final route = _routes.get(filteredSegments);
    if (route == null) {
      _notFound(request, context);
      return;
    }
    await route(request, context);
  }
}
